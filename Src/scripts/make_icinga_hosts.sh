#!/bin/bash

mysql_ip=`docker inspect --format '{{ .NetworkSettings.IPAddress }}' docker_mysql_1`
apache_ip=`docker inspect --format '{{ .NetworkSettings.IPAddress }}' docker_apache_1`

echo "define host{
        use                     generic-host    ; Inherit default values from a template
        host_name               mysql           ; The name we're giving to this host
        alias                   Mysql           ; A longer name associated with the host
        address                 $mysql_ip          ; IP address of the host
        hostgroups              mysql-containers    ; Host groups this host is associated with
    }" > "/etc/icinga/objects/container_mysql.cfg"

echo "define host{
        use                     generic-host    ; Inherit default values from a template
        host_name               apache           ; The name we're giving to this host
        alias                   Apache           ; A longer name associated with the host
        address                 $apache_ip          ; IP address of the host
        hostgroups              web-containers    ; Host groups this host is associated with
    }
" > "/etc/icinga/objects/container_apache.cfg"

echo "<VirtualHost *:*>
    <LocationMatch \"^(?:/icinga|/cgi-bin/icinga)\">
        ProxyPass !
    </LocationMatch>

    ProxyPreserveHost On
    ProxyPass / http://$apache_ip/
    ProxyPassReverse / http://$apache_ip/

    ServerName localhost
</VirtualHost>" > /etc/apache2/sites-enabled/000-default.conf
