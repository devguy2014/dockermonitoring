#!/bin/bash

lockfile=/var/lock/uploadlogs.lock

lockfile-create -r 0 --lock-name $lockfile
lockresult=$?

if [ $lockresult -ne "0" ]; then
    exit 1;
fi

path="/var/log/project"

if [ ! -f /root/project_config ]; then
    echo "Project config file not found"
    lockfile-remove --lock-name $lockfile
    exit 1
fi

. /root/project_config

s3_bucket="$aws_s3_bucket"

export AWS_ACCESS_KEY_ID=$aws_access_key_id
export AWS_SECRET_ACCESS_KEY=$aws_secret_access_key
export PATH=/usr/local/sbin:/usr/local/bin:$PATH

cd $path

files=`/usr/bin/find . -name \*.gz -printf '%P\n'`

for fname in $files; do
    
    if [ -f $fname ]; then
        aws s3 mv $fname "s3://$s3_bucket/$fname"
        result=$?
        if [ $result -ne "0" ]; then
            echo "$(date '+%Y-%m-%d %H:%M:%S') $fname is not uploaded" >> "$path/failure.log"
            lockfile-remove --lock-name $lockfile
            exit 1
        fi
        echo "$(date '+%Y-%m-%d %H:%M:%S') $fname is uploaded" >> "$path/success.log"
    fi
done

for fname in $files; do
    dirname=`dirname $fname`
    if [ -d $dirname ]; then
        rm -rf $dirname
    fi
done

lockfile-remove --lock-name $lockfile
exit 0;