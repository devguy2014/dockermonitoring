#!/bin/bash

/usr/sbin/mysqld &

if [ ! -f /root/setpassword ]; then
    sleep 5
    cat << EOF > /root/user.sql
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
UPDATE user SET password=PASSWORD('$MYSQL_ROOT_PASSWORD') WHERE user='root';
CREATE USER 'icinga'@'%' IDENTIFIED BY '$MYSQL_ICINGA_PASSWORD';
CREATE USER 'modx'@'%' IDENTIFIED BY '$MYSQL_ICINGA_PASSWORD';
GRANT ALL PRIVILEGES ON modx.* TO 'modx'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EOF

    /usr/bin/mysql -uroot < /root/modx.sql
    /usr/bin/mysql -uroot < /root/user.sql
    rm /root/user.sql
    touch /root/setpassword
fi

/usr/sbin/cron -f
