#!/bin/bash

filename=$(date +%Y%m%d_%H%M%S)
dest_path="/root/logs/$filename"
failurelog="/root/logs/failure.log"
successlog="/root/logs/success.log"

mkdir -p $dest_path

/usr/sbin/logrotate -f /root/config/logrotate_apache2.conf
cd /var/log/apache2

for fname in $(ls *.gz); do
    mv $fname $dest_path
    result=$?
    if [ "$result" -ne "0" ]; then
        echo "$(date '+%Y-%m-%d %H:%M:%S') $fname is not moved" >> "$failurelog"
        exit 1
    fi
    echo "$(date '+%Y-%m-%d %H:%M:%S') $fname is moved" >> "$successlog"
done

exit 0

