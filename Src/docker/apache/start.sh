#!/bin/bash


/bin/chmod -R 777 /var/www/html/assets/cache
/bin/chmod -R 777 /var/www/html/assets/images
/bin/chmod -R 777 /var/www/html/assets/export
/bin/chmod -R 777 /var/www/html/assets/files
/bin/chmod -R 777 /var/www/html/assets/flash
/bin/chmod -R 777 /var/www/html/assets/media
/bin/chmod -R 777 /var/www/html/assets/backup
/bin/chmod -R 777 /var/www/html/assets/.thumbs
/bin/chmod -R 777 /var/www/html/manager/includes/config.inc.php

/usr/sbin/sshd
/usr/sbin/cron
/usr/sbin/apache2ctl -D FOREGROUND

