$projectpath = "/home/ubuntu/project"
$packname = "linux-image-extra-$kernelrelease"
$dockercompose = "/usr/bin/curl -L https://github.com/docker/compose/releases/download/1.7.1/docker-compose-$kernel-$hardwaremodel > /usr/local/bin/docker-compose && /bin/chmod +x /usr/local/bin/docker-compose"

$aws_key="AKIAJX3ARM2PGBMKGFEA"
$aws_secret="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"
$s3_bucket="rmukhammadaliyev"

    file { "/etc/apt/sources.list.d/other.list":
        source => "$projectpath/files/other.list",
        before => Package['docker-engine']
    }

    exec { 'icinga-key':
        before => Exec['apt-update'],
        command => '/usr/bin/wget -O - http://packages.icinga.org/icinga.key | /usr/bin/apt-key add -',
        cwd => '/root'
    }

    exec { 'apt-update':
        require => File['/etc/apt/sources.list.d/other.list'],
        command => '/usr/bin/apt-get update'
    }

    package { 'apt-transport-https':
        ensure => 'installed'
    }

    package { 'ca-certificates':
        ensure => 'installed'
    }

    package { "linux-image-extra-$kernelrelease":
        ensure => "installed",
        before => Package['docker-engine']
    }

    package { "docker-engine":
        require => [File['/etc/apt/sources.list.d/other.list'], Exec['apt-update'], Package['apt-transport-https', 'ca-certificates']],
        ensure => "installed"
    }

    service { "docker":
        require => Package["docker-engine"],
        name => "docker",
        ensure => "running"
    }

    user { "ubuntu":
        require => Package["docker-engine"],
        groups => ["docker"],
        membership => "minimum"
    }

    package { "curl":
        ensure => "installed"
    }

    exec { "docker-compose":
        require => Service['docker'],
        command => $dockercompose
    }

    exec { "directory-logs-apache":
        command => "/bin/mkdir -p /var/log/project/apache"
    }

    exec { "directory-logs-mysql":
        command => "/bin/mkdir -p /var/log/project/mysql"
    }

    package { "python-pip":
        require => Exec['apt-update'],
        ensure => "installed"
    }

    exec { "awscli":
        require => Package["python-pip"],
        command => "/usr/bin/pip install awscli"
    }

    exec { "directory-aws":
        command => "/bin/mkdir -p /root/.aws"
    }

    file { "aws-config":
        name => "/root/.aws/config",
        require => Exec['directory-aws'],
        source => "$projectpath/files/aws_config"
    }

    exec { "create_s3_bucket":
        require => [File["aws-credentials", "aws-config"], Exec["awscli"]],
        command => "/usr/local/bin/aws s3 mb s3://$s3_bucket 2>/dev/null"
    }

    file { "aws-credentials":
        name => "/root/.aws/credentials",
        require => Exec['directory-aws'],
        content => "[default]
aws_access_key_id = $aws_key
aws_secret_access_key = $aws_secret"
    }

    file { "project-config":
        name => "/root/project_config",
        before => File["uploadlogs.sh"],
        content => "aws_s3_bucket=$s3_bucket
aws_access_key_id=$aws_key
aws_secret_access_key=$aws_secret
"
    }

    file { "uploadlogs.sh":
        name => "/usr/local/bin/uploadlogs.sh",
        source => "$projectpath/scripts/uploadlogs.sh",
        require => Exec['docker-compose-containers'],
        owner => "root",
        mode => "0755"
    }

    cron {  "cron-uploadlogs":
        require => [Exec['directory-logs-apache', 'directory-logs-mysql'], File['aws-config', 'aws-credentials']],
        command => "/bin/bash /usr/local/bin/uploadlogs.sh",
        user => "root",
        hour => "19",
        ensure => "present"
    }

    exec { "docker-compose-containers":
        require => [User["ubuntu"], Exec["docker-compose"]],
        cwd => "$projectpath/docker",
        command => "/usr/local/bin/docker-compose up -d"
    }

    package { "icinga":
        require => [File['/etc/apt/sources.list.d/other.list'], Exec['icinga-key', 'apt-update']],
        ensure => "installed"
    }

    exec { "icingaadmin":
        require => Package["icinga"],
        command => '/usr/bin/htpasswd -c -b /etc/icinga/htpasswd.users icingaadmin Password'
    }

    exec { "icinga-plugin-files":
        require => Package["icinga"],
        command => "/bin/cp $projectpath/icinga/*.* /etc/icinga/objects/"
    }

    service { "icinga":
        require => Package["icinga"],
        name => 'icinga',
        ensure => "running",
        subscribe => Exec["icinga-plugin-files", "icinga-plugin-containers-files"]
    }

    exec { "icinga-plugin-containers-files":
        require => Exec["icinga-plugin-files"],
        command => "/bin/bash $projectpath/scripts/make_icinga_hosts.sh"
    }

    exec { "apache-modules":
        require => Package['icinga'],
        command => "/usr/sbin/a2enmod proxy proxy_http rewrite deflate headers"
    }

    service { "apache2":
        name => 'apache2',
        ensure => "running",
        subscribe => Exec['apache-modules', 'icinga-plugin-containers-files']
    }

    exec { "webapp-htpasswd":
        require => Exec["apache-modules"],
        command => '/usr/bin/htpasswd -c -b /etc/apache2/htpasswd.users webadmin Password'
    }

